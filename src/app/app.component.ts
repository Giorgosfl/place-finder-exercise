import {Component, OnInit} from '@angular/core';
import {Marker} from '../app/models/Marker'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'place-finder-exercise';
  place: google.maps.places.PlaceResult

  marker: Marker
  mapCenter: google.maps.LatLngLiteral

  ngOnInit() {
    this.initializeCurrentLocation()
  }

  getPlace(place: google.maps.places.PlaceResult) {
    this.place = place
    this.initMarker(place)
  }

  initMarker(place: google.maps.places.PlaceResult) {
    let images = []
    this.marker = new Marker()

    if (!place.geometry) {
      this.initializeCurrentLocation()
      return
    }

    if (place && place.photos) {
      place['photosUrls'] = place.photos.map(photo => photo.getUrl({}))
      images = place['photosUrls'].map(url => {
        return {
          image: url,
          thumbImage: url,
        }
      })
    }


    const location = place.geometry.location.toJSON()
    this.marker = {
      position: location,
      options: {
        // animation: google.maps.Animation.BOUNCE
        icon: {
          url: 'http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png',
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(40, 40)
        }
      },
      place: place,
      images: images

    }

    this.mapCenter = this.marker.position
  }


  initializeCurrentLocation() {
    if (!navigator.geolocation) {
      return
    }

    navigator.geolocation.getCurrentPosition(position => {
      this.mapCenter = {
        lat: position.coords.latitude,
        lng: position.coords.longitude,
      }
    })
  }


}
