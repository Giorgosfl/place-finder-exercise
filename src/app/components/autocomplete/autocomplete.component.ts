import {Component, ViewChild, EventEmitter, Output, OnInit, AfterViewInit, Input} from '@angular/core';
// @ts-ignore
import {} from '@types/googlemaps';
import {Address} from "ngx-google-places-autocomplete/objects/address";

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {

  @Input() placeType: string;
  @Output() setPlace: EventEmitter<any> = new EventEmitter();
  autocompleteInput: any;

  options = {
    // componentRestrictions: {country: ["CY"]},
    // types: [this.placeType]  // 'establishment' / 'address' / 'geocode'
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  public AddressChange(address: Address) {
    this.setPlace.emit(address)
  }
}
