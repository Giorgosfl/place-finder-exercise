import {Component, Input, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {GoogleMap, MapInfoWindow, MapMarker} from "@angular/google-maps";
import {Marker} from "../../models/Marker"


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() place: google.maps.places.PlaceResult
  @Input() marker: Marker
  @Input() center: google.maps.LatLngLiteral
  @ViewChild(MapInfoWindow, {static: false}) infoWindow: MapInfoWindow
  @ViewChild(GoogleMap, {static: false}) googleMap: GoogleMap

  infoWindowIsOpened: boolean = false;
  infoWindowOptions: google.maps.InfoWindowOptions = {pixelOffset: new google.maps.Size(-17, -2)}
  options: google.maps.MapOptions = {
    zoom: 12,
    fullscreenControl: false,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
      position: google.maps.ControlPosition.TOP_RIGHT,
    },
  }

  constructor() {

  }

  ngOnInit(): void {
  }

  mapClicked() {
    if (this.infoWindowIsOpened) {
      this.closeInfo()
    }
  }

  closeInfo() {
    this.infoWindow.close()
    this.infoWindowIsOpened = false
  }

  openInfo(marker: MapMarker) {
    if (this.infoWindowIsOpened) {
      this.closeInfo()
      return
    }

    this.infoWindowOptions.position = marker.position
    this.infoWindow.open(marker)
    this.infoWindowIsOpened = true
  }

  markerMouseHover() {
    this.changeMarkerIcon('http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png')
  }

  markerMouseOut() {
    this.changeMarkerIcon('http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png')
  }

  private changeMarkerIcon(iconUrl) {
    const tempMarker = JSON.parse(JSON.stringify(this.marker))
    Object.assign(tempMarker.options.icon, {url: iconUrl})
    this.marker = tempMarker
  }
}
