import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AutocompleteComponent} from './components/autocomplete/autocomplete.component';
import {FormsModule} from "@angular/forms";
import {GoogleMapsModule} from '@angular/google-maps';
import {MapComponent} from './components/map/map.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {MatCardModule} from "@angular/material/card";
import {NgImageSliderModule} from 'ng-image-slider';
import {GooglePlaceModule} from "ngx-google-places-autocomplete";


@NgModule({
  declarations: [
    AppComponent,
    AutocompleteComponent,
    MapComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    GoogleMapsModule,
    BrowserAnimationsModule,
    MatCardModule,
    NgImageSliderModule,
    GooglePlaceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
