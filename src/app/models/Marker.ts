import {} from "@angular/google-maps";

export class Marker {
  position: google.maps.LatLngLiteral;
  place: google.maps.places.PlaceResult;
  images: Array<object>;
  options: google.maps.MarkerOptions;

  constructor(_position?: google.maps.LatLngLiteral, _place?: google.maps.places.PlaceResult, _images?: Array<object>, _options?: google.maps.MarkerOptions
  ) {
    this.position = _position
    this.options = _options
    this.place = _place
    this.images = _images
  }


}
